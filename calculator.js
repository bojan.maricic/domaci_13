var Calculator = function(){
  this.memory = [];
  this.lastButton = "";
}

Calculator.prototype.clearAll= function(){
  this.memory = [];
  this.lastButton = "";
  display.innerHTML = "";
}

Calculator.prototype.equals = function(){
  if(this.memory.length === 1){
    return
  }
  this.memory.push(parseInt(display.innerHTML))
  this.lastButton = "equals";
  this.performCalculation();
  display.innerHTML = this.memory[0]
}

Calculator.prototype.operate = function(operand){
  if(this.lastButton !== "equals"){
    this.memory.push(parseInt(display.innerHTML))
  }
  
  this.lastButton = "operator";

  if(this.memory.length === 3){
    this.performCalculation()
  }

  this.memory.push(operand);
  display.innerHTML = this.memory[0]
}

Calculator.prototype.performCalculation = function(){
  if(this.memory[1] === "+"){
    this.memory = [this.memory[0] + this.memory[2]];
  } else if(this.memory[1] === "-"){
    this.memory = [this.memory[0] - this.memory[2]];
  } else if(this.memory[1] === "*"){
    this.memory = [this.memory[0] * this.memory[2]];
  } else if(this.memory[1] === "/"){
    this.memory = [this.memory[0] / this.memory[2]];
  } 
}


Calculator.prototype.renderInDisplay = function(buttonHTML){
  if(this.lastButton === "operator"){
    display.innerHTML = ""
    this.lastButtonWasOperator = false;
  }
  display.innerHTML += buttonHTML;
}


var myCalculator = new Calculator();

var numbers = document.querySelectorAll(".number");
var display = document.querySelector(".display");
var operatorButtons = document.querySelectorAll(".operator");
var equalsButton = document.querySelector(".equals");
var clearButton = document.querySelector(".clear");

equalsButton.addEventListener("click", function(){
	myCalculator.equals();
})

clearButton.addEventListener("click", function(){
	myCalculator.clearAll();
})

for (var i = 0; i < numbers.length; i++){
	numbers[i].addEventListener("click", function(){
		console.log(this.innerHTML);
		myCalculator.renderInDisplay(this.innerHTML);
	})
}

for(var i = 0; i < operatorButtons.length; i++){
	operatorButtons[i].addEventListener("click", function(){
		myCalculator.operate(this.innerHTML);
	})
}






